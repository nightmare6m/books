import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Book Finder', () => {
    page.navigateTo();
    expect(page.getHeaderText()).toEqual('Book Finder');
  });
});
