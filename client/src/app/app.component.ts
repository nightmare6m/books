import { Component } from '@angular/core';
import {Subject} from 'rxjs';
import {FilterService} from './filter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  searchTerm$ = new Subject<string>();
  searching = false;
  books=[];
  hasError=false;
  disableNext =true;
  disablePrevious = true;
  pageNumber = 1;
  totalPages = 1;
  currentSearch = "";

  constructor(private filterService: FilterService){
    const setResultsFunction = setResults.bind(this);
    this.filterService.find(this.searchTerm$)
    .subscribe(setResultsFunction);
    this.filterService.hasError.subscribe(hasError=>{
      this.hasError=hasError;
    });
    this.filterService.isLoading.subscribe(isLoading=>{
      this.searching=isLoading;
    });
  }
  inputText(txt: string){
    this.currentSearch = txt;
    this.searchTerm$.next(txt+'&page=1');
  }
  newPage(pageIncrement: number){
    let newPage = this.pageNumber + pageIncrement;
    if(newPage < 1){
      newPage = 1;
    }
    else if(newPage > this.totalPages){
      newPage = this.totalPages;
    }
    this.searchTerm$.next(this.currentSearch+'&page='+newPage);
  }
}

function setResults(data){
  this.books=data.books;
  this.totalPages=Math.max(1, data.totalPages);
  this.pageNumber=Number(data.page);
  this.disablePrevious = this.pageNumber === 1;
  this.disableNext = this.pageNumber === this.totalPages;
}
