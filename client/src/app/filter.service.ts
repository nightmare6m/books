import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable, Subject} from 'rxjs';
import {map, debounceTime, distinctUntilChanged, switchMap, catchError} from 'rxjs/operators';


@Injectable()
export class FilterService{
  url = '/api?filter=';
  hasError: Subject<boolean>;
  isLoading: Subject<boolean>;
  constructor(private http: Http){
    this.isLoading=new Subject();
    this.hasError=new Subject();
  }
  find(terms: Observable<string>){
    return terms.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(searchTerm => this.searchForTerm(searchTerm))
    );
  }
  searchForTerm(searchTerm){
    this.hasError.next(false);
    this.isLoading.next(true);
    return this.http.get(this.url+searchTerm).pipe(
      map(res => {
        this.hasError.next(false);
        this.isLoading.next(false);
        return res.json()
      }),
      catchError(this.handleError.bind(this))
    );
  }
  handleError(e){
    this.hasError.next(true);
    this.isLoading.next(false);
    return [];
  }
}
