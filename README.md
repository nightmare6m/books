# Books App
Use this app to search for books. The search results will be displayed alphabetized by title showing 100 results per page.

## Steps to Run
- Clone this repo
- cd client
- npm install
- npm run build
- cd ../
- npm install
- npm run start
- navigate to localhost:3000
