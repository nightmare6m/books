const express = require('express');
const request = require('request-promise-native');
const app = express();

const bookApi = 'http://openlibrary.org/search.json?q=';
const coverUrl = 'http://covers.openlibrary.org/b/id/COVERID-M.jpg';
const placeholderCover = '/assets/unavailable.jpg';
const resultsPerPage = 100;
const maxPages = 10;//limit number of results
const cache ={};

function sorter(a, b){
  return a.title.toLowerCase().localeCompare(b.title.toLowerCase());
}
function bookMap(doc){
  const cover = doc.cover_i?coverUrl.replace('COVERID', doc.cover_i):placeholderCover;
  return {
    title: doc.title,
    author: doc.author_name,
    published: doc.first_publish_year,
    coverUrl: cover
  };
}
function getBooks(books, start, end){
  return books.slice(start, end).map(bookMap);
}

app.use(express.static(__dirname + '/client/dist/book-list-fe'));

app.get('/api', (req, res) => {
    const page = req.query.page?req.query.page:1;
    const start = (page-1) * resultsPerPage;
    const end = start + resultsPerPage;

    if(cache[req.query.filter]){
      const storedData = cache[req.query.filter];
      const books = getBooks(storedData.books, start, end);
      return res.status(200).send({
        books,
        page: page,
        totalPages: storedData.totalPages
      });
    }
    request(bookApi+req.query.filter+'&page='+page).then(body=>{
      const bodyJson = JSON.parse(body);
      const totalPages = Math.min(maxPages, Math.ceil(bodyJson.numFound/resultsPerPage));
      let promises = [];
      for(let i =0; i< totalPages; i++){
        promises.push(request(bookApi+req.query.filter+'&page='+(i+1)));
      }

      Promise.all(promises).then(pages =>{
        let books =[];
        pages.forEach(page=>{
          books=books.concat(JSON.parse(page).docs);
        });
        books=books.sort(sorter);
        cache[req.query.filter]={
          books,
          totalPages
        };
        return res.status(200).send({
          books: getBooks(books, start, end),
          page: page,
          totalPages: totalPages
        });
      })
      .catch(e=>{
        return res.status(500).send('There was an error on the server');
      });
    })
    .catch(e=>{
      return res.status(500).send('There was an error on the server');
    });
  }
);

app.get('*', (req, res) => {
  res.redirect('/index.html');
});

app.listen(3000, () => console.log('listening on port 3000!'))
